FROM archlinux:latest

# SETUP WORKDIR
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# INSTALL BASE PACKAGES
COPY packages.txt .
RUN pacman -Syu --noconfirm
RUN pacman -S --noconfirm --needed - < packages.txt
RUN rm packages.txt

# CLEANUP
RUN sudo pacman -Scc --noconfirm && sudo pacman -Rns --noconfirm $(pacman -Qtdq) || echo 'Nothing to remove'
